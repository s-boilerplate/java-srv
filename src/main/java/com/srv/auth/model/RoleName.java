package com.srv.auth.model;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
