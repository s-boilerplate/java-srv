package com.srv.auth.model.mapper;

import com.srv.auth.model.User;
import com.srv.auth.payload.Profile;
import com.srv.dblayer.entity.DbPostEntity;
import com.srv.domainlayer.mapper.DoBaseMapper;
import com.srv.uilayer.payload.UiPost;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserMapper extends DoBaseMapper<Profile, User> {
    UserMapper MAPPER = Mappers.getMapper( UserMapper.class );

    UiPost createFromDb(DbPostEntity source);
    DbPostEntity createFromUi(UiPost source);
}