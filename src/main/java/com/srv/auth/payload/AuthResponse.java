package com.srv.auth.payload;

import com.fasterxml.jackson.annotation.JsonInclude;

public class AuthResponse {
    private Object user;
    private Object token;

    public Object getUser() {
        return user;
    }

    public void setUser(Object user) {
        this.user = user;
    }

    public Object getToken() {
        return token;
    }

    public void setToken(Object token) {
        this.token = token;
    }
}
