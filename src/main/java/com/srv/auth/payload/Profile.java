package com.srv.auth.payload;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.srv.auth.model.Role;
import org.hibernate.annotations.NaturalId;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE, creatorVisibility = Visibility.NONE)
@JsonPropertyOrder(value = {"id", "firstname", "lastname", "address", "phone", "birthday", "email", "_created", "_updated"})
public class Profile {

    @JsonProperty("id")
    private UUID mId;

    @JsonProperty("firstname")
    private String mFirstName;

    @JsonProperty("lastname")
    private String mLastName;

    @JsonProperty("address")
    private String mAddress;

    @JsonProperty("phone")
    private String mPhone;

    @JsonProperty("birthday")
    private String mBirthday;

    @JsonProperty("email")
    private String mEmail;

    @JsonProperty("_created")
    private Date mDateCreated;

    @JsonProperty("_updated")
    private Date mDateUpdated;

    @JsonIgnore
    private List<Role> roles;

    public Profile() {
    }

    public Profile(UUID pId, String pFirstName, String pLastName, String pAddress, String pPhone, String pBirthday, String pEmail, String pPassword, String pRole, Date pDateCreated, Date pDateUpdated) {
        this.mId = pId;
        this.mFirstName = pFirstName;
        this.mLastName = pLastName;
        this.mAddress = pAddress;
        this.mPhone = pPhone;
        this.mBirthday = pBirthday;
        this.mEmail = pEmail;
        this.mDateCreated = pDateCreated;
        this.mDateUpdated = pDateUpdated;
    }

    public UUID getId() {
        return mId;
    }

    public void setId(UUID pId) {
        this.mId = pId;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String pFirstName) {
        this.mFirstName = pFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String pLastName) {
        this.mLastName = pLastName;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String pAddress) {
        this.mAddress = pAddress;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String pPhone) {
        this.mPhone = pPhone;
    }

    public String getBirthday() {
        return mBirthday;
    }

    public void setBirthday(String pBirthday) {
        this.mBirthday = pBirthday;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String pEmail) {
        this.mEmail = pEmail;
    }

    public Date getDateCreated() {
        return mDateCreated;
    }

    public void setDateCreated(Date pDateCreated) {
        this.mDateCreated = pDateCreated;
    }

    public Date getDateUpdated() {
        return mDateUpdated;
    }

    public void setDateUpdated(Date pDateUpdated) {
        this.mDateUpdated = pDateUpdated;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
