package com.srv.auth.repository;

import com.srv.auth.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    Role findByName(String name);

    @Override
    void delete(Role role);

}
