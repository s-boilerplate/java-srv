package com.srv.auth.repository;

import com.srv.auth.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    User findByEmail(String email);
    Boolean existsByUsername(String email);
    Boolean existsByEmail(String email);

    @Override
    void delete(User user);

}
