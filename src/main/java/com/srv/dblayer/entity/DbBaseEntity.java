package com.srv.dblayer.entity;


import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;


@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class DbBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "_id", columnDefinition = "BINARY(16)", updatable = false)
    private UUID id;

    @Column(name = "_created", nullable = true, updatable = false)
    @CreatedDate
    private Date created;

    @Column(name = "_updated")
    @LastModifiedDate
    private Date updated;

    @Column(name = "_owner")
    @CreatedBy
    private String owner;

    @Column(name = "_updater")
    @LastModifiedBy
    private String updater;

    @Column(name = "_deleted", columnDefinition = "BOOL NOT NULL DEFAULT FALSE", insertable = false, updatable = false)
    private Boolean deleted;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
