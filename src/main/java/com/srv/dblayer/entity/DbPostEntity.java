package com.srv.dblayer.entity;

import com.srv.uilayer.payload.UiPost;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity()
@Table(name = "post")
public class DbPostEntity extends DbBaseEntity {

    @Column(name = "title")
    public String title;

    @Column(name = "body")
    public String body;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return body;
    }

    public void setText(String body) {
        this.body = body;
    }
}
