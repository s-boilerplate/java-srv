package com.srv.dblayer.repository;

import com.srv.dblayer.entity.DbBaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DbBaseRepository<T extends DbBaseEntity> extends JpaRepository<T, UUID> {
}
