package com.srv.dblayer.repository;

import com.srv.dblayer.entity.DbPostEntity;
import org.springframework.stereotype.Repository;

@Repository("post")
public interface DbPostRepository extends DbBaseRepository<DbPostEntity>{
}
