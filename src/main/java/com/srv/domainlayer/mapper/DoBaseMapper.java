package com.srv.domainlayer.mapper;

import org.mapstruct.factory.Mappers;

public interface DoBaseMapper<U, D> {
    DoBaseMapper INSTANCE = Mappers.getMapper( DoBaseMapper.class );

    U createFromDb(D source);
    D createFromUi(U source);
}