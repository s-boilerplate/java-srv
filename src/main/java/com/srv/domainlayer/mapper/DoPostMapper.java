package com.srv.domainlayer.mapper;

import com.srv.dblayer.entity.DbPostEntity;
import com.srv.uilayer.payload.UiPost;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface DoPostMapper extends DoBaseMapper<UiPost, DbPostEntity> {
    DoPostMapper MAPPER = Mappers.getMapper( DoPostMapper.class );

    UiPost createFromDb(DbPostEntity source);
    DbPostEntity createFromUi(UiPost source);
}