package com.srv.domainlayer.service;

import java.util.List;

public interface DoBaseInterfaceService<U, ID> {
    public List<U> getAll();
    public List<U> getAllShare(String email);

    public U get(ID pId) throws Exception;
    public U insert(U pObject) throws Exception;
    public U update(U pObject, ID pId) throws Exception;
    public void delete(ID pId) throws Exception;

    public void validate(U pObject);
}
