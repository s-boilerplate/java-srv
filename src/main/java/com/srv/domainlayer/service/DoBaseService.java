package com.srv.domainlayer.service;

import com.srv.dblayer.entity.DbBaseEntity;
import com.srv.dblayer.repository.DbBaseRepository;
import com.srv.domainlayer.mapper.DoBaseMapper;
import com.srv.exception.NotFoundException;
import com.srv.utils.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class DoBaseService<U, D extends DbBaseEntity>  implements DoBaseInterfaceService<U, UUID> {

    @Autowired
    DbBaseRepository<D> mRepository;

    @Autowired
    DoBaseMapper<U, D> mMapper;

    Logger logger = LoggerFactory.getLogger(DoBaseService.class);

    @Override
    public List<U> getAll() {
        List<D> listFromDb = mRepository.findAll();
        return listFromDb.stream().map(item -> mMapper.createFromDb(item)).collect(Collectors.toList());
    }

    @Override
    public List<U> getAllShare(String email) {
        return null;
    }

    @Override
    public U get(UUID pId) throws Exception {
        D entity = mRepository.findById(pId).orElseThrow(() -> new NotFoundException(pId));
        return mMapper.createFromDb(entity);
    }

    @Override
    public U insert(U pObject) throws Exception {
        // Create entity from ui object
        D entity = mMapper.createFromUi(pObject);
        // Clear the id
        entity.setId(null);
        // Insert to db
        entity = mRepository.saveAndFlush(entity);
        // Map everything back to ui
        return mMapper.createFromDb(entity);
    }

    @Override
    public U update(U pObject, UUID pId) throws Exception {
        D olDEntity = mRepository.findById(pId).orElseThrow(() -> new NotFoundException(pId));
        D newEntity = mMapper.createFromUi(pObject);
        newEntity.setId(null);

        D entity = AppUtils.combine2Objects(olDEntity, newEntity);

        // Save to db
        entity = mRepository.saveAndFlush(entity);
        // Map everything back to ui
        return mMapper.createFromDb(entity);
    }

    @Override
    public void delete(UUID pId) throws Exception {

    }

    @Override
    public void validate(U pObject) {

    }
}
