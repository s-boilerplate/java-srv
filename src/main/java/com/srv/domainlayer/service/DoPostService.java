package com.srv.domainlayer.service;

import com.srv.dblayer.entity.DbPostEntity;
import com.srv.uilayer.payload.UiPost;
import org.springframework.stereotype.Service;

@Service
public class DoPostService extends DoBaseService<UiPost, DbPostEntity> {
}
