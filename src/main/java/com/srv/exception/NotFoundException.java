package com.srv.exception;

import java.util.function.Supplier;

public class NotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    private static final String MESSAGE_TEMPLATE = "Entity %s not found!";

    public NotFoundException(Object pId) {
        super(String.format(MESSAGE_TEMPLATE, String.valueOf(pId)));
    }
}
