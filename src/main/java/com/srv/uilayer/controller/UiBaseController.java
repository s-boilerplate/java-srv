package com.srv.uilayer.controller;

import com.srv.dblayer.entity.DbBaseEntity;
import com.srv.domainlayer.mapper.DoBaseMapper;
import com.srv.domainlayer.service.DoBaseService;
import com.srv.exception.NotFoundException;
import com.srv.uilayer.payload.UiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

public class UiBaseController<U, D extends DbBaseEntity> {

    @Autowired
    DoBaseService mService;

    @GetMapping()
    public ResponseEntity<UiResponse> getAll() {
        UiResponse response = new UiResponse();
        try {
            response.setData(mService.getAll());
        } catch (Exception e) {
            response.setInternalError(e);
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, response.getStatus());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UiResponse> getItem(@PathVariable("id") UUID id) {
        UiResponse response = new UiResponse();
        try {
            response.setData(mService.get(id));
        } catch (NotFoundException e) {
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setMessage(e.getMessage());
        } catch (Exception e) {
            response.setInternalError(e);
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, response.getStatus());
    }

    @PostMapping()
    public ResponseEntity<UiResponse> createItem(@Valid @RequestBody U payload) {
        UiResponse response = new UiResponse();
        try {
            response.setData(mService.insert(payload));
        } catch (Exception e) {
            response.setInternalError(e);
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, response.getStatus());
    }

    @PatchMapping("/{id}")
    public ResponseEntity<UiResponse> updateItem(@PathVariable("id") UUID id, @Valid @RequestBody U payload) {
        UiResponse response = new UiResponse();
        try {
            response.setData(mService.update(payload, id));
        } catch (NotFoundException e) {
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setMessage(e.getMessage());
        } catch (Exception e) {
            response.setInternalError(e);
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, response.getStatus());
    }
}
