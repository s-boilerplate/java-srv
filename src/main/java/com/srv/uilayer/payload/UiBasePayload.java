package com.srv.uilayer.payload;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.UUID;

public class UiBasePayload {
    @JsonProperty("_id")
    private UUID id;

    @JsonProperty("_created")
    private Date created;

    @JsonProperty("_updated")
    private Date updated;

    @JsonProperty("_owner")
    private String owner;

    @JsonProperty("_updater")
    private String updater;

    public UiBasePayload(){
    }

    public UiBasePayload(UUID pId, String pOwner, Date pCreated, Date pUpdated){
        this.id = pId;
        this.owner = pOwner;
        this.created = pCreated;
        this.updated = pUpdated;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }
}
