package com.srv.uilayer.payload;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.http.HttpStatus;

@JsonPropertyOrder(value = {"status", "message", "data", "metadata"})
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE, creatorVisibility = Visibility.NONE)
public class UiResponse {
    @JsonProperty("status")
    private HttpStatus status = HttpStatus.OK;

    @JsonProperty("message")
    private String message = "Your request has been successful";

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("data")
    private Object data;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("metadata")
    private Object metadata;

    public UiResponse() {}

    public UiResponse(Object data) {
        this.data = data;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    public void setInternalError(Exception pExeption) {
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
        this.message = pExeption.getMessage();
    }

}
